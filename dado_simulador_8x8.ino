//Librería para manejar la matriz con controlador MAX72XX
#include "LedControl.h"

//Librería: https://github.com/wayoda/LedControl
/*
Esquema de conexión de pines (Probado en Arduino UNO)
 pin 12 conectado a DataIn 
 pin 11 conectado a CLK 
 pin 10 conectado a LOAD 
 Utilizamos una única Matriz
 */
LedControl matrizLED=LedControl(12,11,10,1);



void setup() {
  /*
   Sacamos el MAX72XX del modo de Ahorro de Energía,
   hacemos una "wakeup call"
   */
  matrizLED.shutdown(0,false);
  /* Ajustamos el brillo */
  matrizLED.setIntensity(0,8);
  /* limpiamos la matriz */
  matrizLED.clearDisplay(0);
}


//Generador de números del dado
void numeroDado(int numero)
{
  switch(numero)
  {
    case 1:
      matrizLED.setRow(0,3, B00011000);
      matrizLED.setRow(0,4, B00011000);
      break;

    case 2:
      matrizLED.setRow(0,0, B11000000);
      matrizLED.setRow(0,1, B11000000);
      matrizLED.setRow(0,6, B00000011);
      matrizLED.setRow(0,7, B00000011);
      break;

    case 3:
      matrizLED.setRow(0,0, B11000000);
      matrizLED.setRow(0,1, B11000000);
      matrizLED.setRow(0,3, B00011000);
      matrizLED.setRow(0,4, B00011000);
      matrizLED.setRow(0,6, B00000011);
      matrizLED.setRow(0,7, B00000011);
      break;

    case 4:
      matrizLED.setRow(0,0, B11000011);
      matrizLED.setRow(0,1, B11000011);
      matrizLED.setRow(0,6, B11000011);
      matrizLED.setRow(0,7, B11000011);
      break;

    case 5:
      matrizLED.setRow(0,0, B11000011);
      matrizLED.setRow(0,1, B11000011);
      matrizLED.setRow(0,3, B00011000);
      matrizLED.setRow(0,4, B00011000);
      matrizLED.setRow(0,6, B11000011);
      matrizLED.setRow(0,7, B11000011);
      break;

    case 6:
      matrizLED.setRow(0,0, B11000011);
      matrizLED.setRow(0,1, B11000011);
      matrizLED.setRow(0,3, B11000011);
      matrizLED.setRow(0,4, B11000011);
      matrizLED.setRow(0,6, B11000011);
      matrizLED.setRow(0,7, B11000011);
      break;
    
  }
}

/*Función para probar 
la visualización de los números*/
void test()
{
  for(int j=1; j<=6; j++)
  {
    numeroDado(j);
    delay(1000);
    matrizLED.clearDisplay(0);
  }
}
  


void loop() 
{ 
  //Función de Prueba
  test();

  for(int i=0; i<999; i++)
  {
    numeroDado(int(random(1,7)));
    delay(1000);
    matrizLED.clearDisplay(0);
  }

}
