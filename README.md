# Arduino-dado-matriz8x8

Proyecto utilizando Arduino (probado con un Nano v3), que simula el lanzamiento de dados, utilizando para mostrar los resultados una matriz LED de 8x8.

Se emplea la librería LedControl.h, que puede descargarse de https://github.com/wayoda/LedControl

La implementación tiene una función de test() para mostrar la totalidad de los números que pueden respresentarse (del 1 al 6)